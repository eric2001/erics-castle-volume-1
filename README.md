# Eric's Castle Volume 1
![Eric's Castle Volume 1](./screenshots/EricsCastleVol1Snap1.jpg) 

## Description
This is probably the first program I ever created.  It was developed on a computer running Mac OS 7.5.3 using a program called [World Builder](http://en.wikipedia.org/wiki/World_Builder).  It's a mostly text based adventure game (with some really bad black and white graphics) where you explore a castle.  The screenshots below should give you a better idea about the game.

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Screenshots
Introduction Screen:

![Eric's Castle Volume 1](./screenshots/EricsCastleVol1Snap1.jpg)

Outside the castle:

![Eric's Castle Volume 1](./screenshots/EricsCastleVol1Snap2.jpg)

At the store:

![Eric's Castle Volume 1](./screenshots/EricsCastleVol1Snap3.jpg)

At the library:

![Eric's Castle Volume 1](./screenshots/EricsCastleVol1Snap4.jpg)

## Instructions
This is an image file of the floppy disc the program was stored on.  It won't run on a modern Macintosh, but if you've got some old hardware lying around feel free to download it.

## History
I believe I started working on this in mid-1997.  Based on the file's last modified timestamp, it looks like I stopped working on it on April 11th, 1998.
